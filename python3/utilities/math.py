
def clamp(value, minimum, maximum):
    return min(max(value, minimum), maximum)