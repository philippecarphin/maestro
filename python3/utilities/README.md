"Utilities" scripts are generally useful in any Python project. They can be copied without modification to other Python projects. There is no project specific code in files in the utilities folder.

Utilities have few, if any, non-native dependencies. They are generally simple support functions. There are no constants or project strings/data in these files.
