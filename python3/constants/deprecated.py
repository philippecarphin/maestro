
"""
This file contains constants describing the old and new for anything deprecated.
"""


"""
The key is deprecated, the value is its equivalent recommended replacement.
"""
OLD_SEQ_VARIABLES={"SEQ_PACKAGE_HOME":"MAESTRO_PACKAGE_HOME",
                   "SEQ_MAESTRO_DOMAIN":"MAESTRO_DOMAIN",
                   "SEQ_MANAGER_DOMAIN":"MAESTRO_DOMAIN",
                   "SEQ_XFLOW_DOMAIN":"MAESTRO_DOMAIN",
                   "SEQ_UTILS_DOMAIN":"MAESTRO_DOMAIN",
                   "SEQ_MAESTRO_SHORTCUT":"MAESTRO_SHORTCUT",
                   "SEQ_BIN":"MAESTRO_BIN",
                   "SEQ_MANAGER_BIN":"MAESTRO_BIN",
                   "SEQ_XFLOW_BIN":"MAESTRO_BIN",
                   "SEQ_UTILS_BIN":"MAESTRO_BIN",
                   "SEQ_TCL_BIN":"MAESTRO_TCL_BIN",
                   "SEQ_SRC":"MAESTRO_SRC",
                   "SEQ_MANAGER_SRC":"MAESTRO_MANAGER_SRC",
                   "SEQ_TCL_SRC":"MAESTRO_TCL_SRC",
                   "SEQ_TCL_LIB":"MAESTRO_TCL_LIB",
                   "SEQ_WRAPPERS":"MAESTRO_WRAPPERS",
                   "SEQ_MAESTRO_VERSION":"MAESTRO_VERSION"}
