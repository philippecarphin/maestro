## AUTHOR

Maestro and its tools were written by the Canadian Meteorological Centre.

## KNOWLEDGE MANAGEMENT

This man page aims to be the one-and-only source of basic usage information on this tool. It describes how things are working right now - not future features.

Please do not duplicate the content of this man page in wiki pages, training documents, or `-h` and `--help` options. Instead, for all those sources direct users to run the `man` command. Note that we can guarantee access to these man pages for anyone who has access to the Maestro tools. This is not necessarily the case for intranet wikis or training resources.

If in the future we decide man pages are not the best way to centralise our information, then:

* Remove all information from this man page.
* Write a new man page which has no information, except for redirecting users to that new source of information.
* Include this section about knowledge management in the new documentation.

## REPORTING ISSUES

If something appears out of date or wrong, please help the team by notifying us, or correcting this man page!

Report Maestro bugs to https://gitlab.science.gc.ca/cmoi/maestro/issues

Contact stuart.spence@canada.ca for questions about the Maestro man pages.

## COPYRIGHT

Maestro and its tools are licensed under GPL 2.1. This is free software: you are free to change and redistribute it.

## SEE ALSO

https://wiki.cmc.ec.gc.ca/wiki/Maestro

https://gitlab.science.gc.ca/cmoi/maestro
