mserver -- launch a Maestro server process
=============================================

## SYNOPSIS

`mserver`

## DESCRIPTION

`mserver` launches a Maestro server process using the configuration XML typically located in `~/.suites/mconfig.xml`. This command can be used to launch multiple mserver processes, which is usually undesirable. If you only want one mserver running, consider running `mserver_check` instead.

For more information on Maestro, see https://wiki.cmc.ec.gc.ca/wiki/Maestro
